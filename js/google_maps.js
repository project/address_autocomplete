(function (Drupal, $, once) {
  Drupal.behaviors.address_autocomplete_google_maps = {
    attach: function (context, settings) {
      once('initiate-autocomplete', 'input.address-line1', context).forEach(function (element) {
        // Create an autocomplete session token and add it into the
        // autocomplete path.
        // @see https://developers.google.com/maps/documentation/places/web-service/autocomplete#sessiontoken
        const autocompleteUrl = new URL(element.getAttribute('data-autocomplete-path'), document.location);
        autocompleteUrl.searchParams.append('session_token', self.crypto.randomUUID());
        element.setAttribute('data-autocomplete-path', autocompleteUrl.toString());

        $(element).data('ui-autocomplete').options.select = function (event, ui) {
          event.preventDefault();
          const adddressDetailsUrl = new URL('/admin/address_autocomplete/address_details', document.location);
          adddressDetailsUrl.searchParams.append('address_id', ui.item.address_id);
          adddressDetailsUrl.searchParams.append('session_token', ui.item.session_token);
          fetch(adddressDetailsUrl)
            .then((response) => response.json())
            .then((data) => {
              // Set form field values.
              const formWrapper = $(element).closest('.js-form-wrapper');
              formWrapper.find('select.administrative-area, input.administrative-area').val(data.administrative_area);
              formWrapper.find('input.locality').val(data.locality);
              formWrapper.find('input.dependent-locality').val(data.dependent_locality);
              formWrapper.find('input.postal-code').val(data.postal_code);
              $(element).val(data.address_line1);

              // Update session token in autocomplete path with a fresh token.
              autocompleteUrl.searchParams.set('session_token', self.crypto.randomUUID());
              element.setAttribute('data-autocomplete-path', autocompleteUrl.toString());
            }
          );
        };
      });
    }
  };
}(Drupal, jQuery, once));
