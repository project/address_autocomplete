<?php

namespace Drupal\address_autocomplete\Element;

use Drupal\address\Element\Address;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an address_autocomplete form element.
 *
 * Usage example:
 *
 * @code
 * $form['address_autocomplete'] = [
 *   '#type' => 'address_autocomplete',
 * ];
 * @endcode
 *
 * @FormElement("address_autocomplete")
 */
class AddressAutocomplete extends Address implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs an AddressAutocomplete object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * @inheritDoc
   */
  public function getInfo() {
    $info = parent::getInfo();

    $info['#process'][] = [
      get_class($this),
      'processAutocomplete',
    ];

    // Attachments.
    $settings = $this->configFactory->get('address_autocomplete.settings');
    switch ($settings->get('active_plugin')) {
      case 'google_maps':
        $info['#attached']['library'][] = 'address_autocomplete/google_maps';
        break;

      default:
        $info['#attached']['library'][] = 'address_autocomplete/address_autocomplete';

    }

    return $info;
  }

  /**
   * @inheritDoc
   */
  public static function processAutocomplete(&$element, FormStateInterface $form_state, &$complete_form) {
    $element["address_line1"]['#autocomplete_route_name'] = 'address_autocomplete.addresses';
    $element["address_line1"]["#attributes"]['placeholder'] = t('Please start typing your address...');
    $country_code = !empty($element["#default_value"]["country_code"]) ? $element["#default_value"]["country_code"] : NULL;
    if (!empty($element["#value"]["country_code"])) {
      $country_code = $element["#value"]["country_code"];
    }
    $element['address_line1']['#autocomplete_route_parameters']['country'] = $country_code;

    return $element;
  }

}
