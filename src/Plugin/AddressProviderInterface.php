<?php

namespace Drupal\address_autocomplete\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines an interface for Address provider plugins.
 */
interface AddressProviderInterface extends PluginFormInterface, PluginInspectionInterface, ConfigurableInterface, ContainerFactoryPluginInterface {

  /**
   * @inheritDoc
   */
  public function processQuery($string);

  /**
   * Process an address details query.
   *
   * @param string $address_id
   *   The ID of the address autocomplete option for which to get details.
   * @param string $session_token
   *   The autocomplete session token, if any (used in Google Maps).
   *
   * @return array
   *   An array containing the address data parts for the given address ID.
   */
  public function processAddressDetailsQuery($address_id, $session_token = NULL);

}
