<?php

namespace Drupal\address_autocomplete\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Address provider plugins.
 */
abstract class AddressProviderBase extends PluginBase implements AddressProviderInterface {

  /**
   * The Guzzle HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * @inheritDoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
    $this->client = $client;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    if (empty($configuration)) {
        // Load runtime config for this address provider.
        $configuration = $container
          ->get('config.factory')
          ->get('address_autocomplete.settings')
          ->get($plugin_id);
        $configuration ??= [];
    }

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
   * @inheritDoc
   */
  public function defaultConfiguration() {
    return [
      'plugin_id' => $this->pluginId,
    ];
  }

  /**
   * @inheritDoc
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * @inheritDoc
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * @inheritDoc
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * @inheritDoc
   */
  public function processAddressDetailsQuery($address_id, $session_token = NULL) {
    return [
      'administrative_area' => NULL,
      'locality' => NULL,
      'dependent_locality' => NULL,
      'postal_code' => NULL,
      'address_line1' => NULL,
    ];
  }

}
