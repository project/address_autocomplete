<?php

namespace Drupal\address_autocomplete\Plugin\AddressProvider;

use Drupal\address_autocomplete\Plugin\AddressProviderBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a GoogleMaps plugin for address_autocomplete
 *
 * @AddressProvider(
 *   id = "google_maps",
 *   label = @Translation("Google Maps"),
 * )
 */
class GoogleMaps extends AddressProviderBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @inheritDoc
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id, $plugin_definition
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->languageManager = $container->get('language_manager');

    return $instance;
  }

  /**
   * @inheritDoc
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'api_key' => '',
      ];
  }

  /**
   * @inheritDoc
   */
  public function processQuery($string) {
    $results = [];

    list($input, $country, $session_token) = explode('||', $string);

    // @see https://developers.google.com/maps/documentation/places/web-service/autocomplete
    $url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';
    $query = [
      // Supply what has been typed so far into the address line 1 field.
      'input' => $input,
      // Filter to results within selected country.
      'components' => $country ? "country:$country" : NULL,
      // Filter to just addresses.
      'types' => 'address',
      // Turn location bias off (or we'd bias to addresses close to the web
      // server's geolocated IP).
      'location' => '0,0',
      'radius' => 1,
      // Get results back in user's current language.
      'language' => $this->languageManager->getCurrentLanguage()->getId(),
      // Group this request into the current autocomplete session.
      'sessiontoken' => $session_token,
      // Supply the configured API key.
      'key' => $this->configuration['api_key'],
    ];

    $response = $this->client->request('GET', $url, [
      'query' => $query,
    ]);

    $content = Json::decode($response->getBody());

    if (empty($content['error_message'])) {
      foreach ($content['predictions'] as $prediction) {
        $results[] = [
          'label' => $prediction['description'],
          'address_id' => $prediction['place_id'],
          'session_token' => $session_token,
        ];
      }
    }

    return $results;
  }

  /**
   * @inheritDoc
   */
  public function processAddressDetailsQuery($address_id, $session_token = NULL) {
    $result = parent::processAddressDetailsQuery($address_id, $session_token);

    // @see https://developers.google.com/maps/documentation/places/web-service/details
    $url = 'https://maps.googleapis.com/maps/api/place/details/json';
    $query = [
      'place_id' => $address_id,
      'fields' => 'name,address_component',
      'language' => $this->languageManager->getCurrentLanguage()->getId(),
      'sessiontoken' => $session_token,
      'key' => $this->configuration['api_key'],
    ];
    $response = $this->client->request('GET', $url, [
      'query' => $query,
    ]);
    $content = Json::decode($response->getBody());

    if (empty($content['error_message'])) {
      foreach ($content['result']['address_components'] as $address_component) {
        if (in_array('administrative_area_level_1', $address_component['types'])) {
          $result['administrative_area'] = $address_component['short_name'];
        }
        elseif (in_array('locality', $address_component['types'])) {
          $result['locality'] = $address_component['long_name'];
        }
        elseif (in_array('sublocality_level_1', $address_component['types'])) {
          $result['dependent_locality'] = $address_component['long_name'];
        }
        elseif (in_array('postal_code', $address_component['types'])) {
          $result['postal_code'] = $address_component['short_name'];
        }
      }
      $result['address_line1'] = $content['result']['name'];
    }

    return $result;
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => t('API Key'),
      '#default_value' => $this->configuration['api_key'],
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $configuration['api_key'] = $form_state->getValue('api_key');
    $this->setConfiguration($configuration);
  }

}
