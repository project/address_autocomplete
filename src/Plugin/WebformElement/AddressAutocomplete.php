<?php

namespace Drupal\address_autocomplete\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\Address;

/**
 * Provides an autocomplete 'address' element.
 *
 * @WebformElement(
 *   id = "address_autocomplete",
 *   label = @Translation("Advanced address with autocomplete"),
 *   description = @Translation("Provides advanced element for storing, validating and displaying international postal addresses (with autocomplete)."),
 *   category = @Translation("Composite elements"),
 *   composite = TRUE,
 *   multiline = TRUE,
 *   states_wrapper = TRUE
 * )
 *
 * @see \Drupal\address_autocomplete\Element\AddressAutocomplete
 */
class AddressAutocomplete extends Address {

}
